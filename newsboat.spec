Name:           newsboat
Version:        2.20.1
Release:        1%{?dist}
Summary:        An RSS/Atom feed reader for text terminals

License:        MIT
URL:            https://newsboat.org/
Source0:        https://github.com/%{name}/%{name}/archive/r%{version}/%{name}-%{version}.tar.gz

BuildRequires:  rust >= 1.25
BuildRequires:  cargo >= 1.25
BuildRequires:  asciidoctor
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  json-c-devel
BuildRequires:  libcurl-devel
BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
BuildRequires:  ncurses-devel
BuildRequires:  pkgconfig
BuildRequires:  sqlite-devel
BuildRequires:  stfl-devel

%description
Newsboat is a fork of Newsbeuter, an RSS/Atom feed reader for the text
console. The only difference is that Newsboat is actively maintained while
Newsbeuter isn't.

%prep
%autosetup -n %{name}-r%{version}
find contrib/ -type f -exec chmod -x {} \;

%build
export CXXFLAGS="${CXXFLAGS:-%optflags}"
./config.sh
%make_build prefix=%{_prefix}

%install
%make_install prefix=%{_prefix}
rm -rf %{buildroot}/%{_datadir}/doc
%find_lang %{name}

%files -f %{name}.lang
%license LICENSE
%doc CHANGELOG.md contrib/ doc/example-config doc/xhtml/* README.md TODO
%{_bindir}/*
%attr(0644,root,root) %{_mandir}/man1/*

%changelog
